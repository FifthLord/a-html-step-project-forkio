import replace from "gulp-replace";//пошук та заміна
import plumber from 'gulp-plumber';//обробка помилок
import notify from 'gulp-notify';//вивід повідомлень (підсказок)
import browsersync from 'browser-sync'//локальний сервер
import newer from "gulp-newer"//перевірка оновлення картинки
import ifPlugin from "gulp-if"//умовне вітвління (для розділення на dev й prod)

export const plugins = {
   replace: replace,
   plumber: plumber,
   notify: notify,
   browsersync: browsersync,
   newer: newer,
   if: ifPlugin,
}