# Pet-project для вдосконалення навичок роботи з адаптивною версткою та препроцесорами SASS SCSS

---

## При розробці проекту використовувались такі технології:
* Методолгія найменування класів BEM;
* HTML з використанням семантичних тегів;
* Принцип Mobile first;
* CSS препроцесор SASS SCSS;
    * _Media query_
    * _Variables_
    * _Mixin_
* Adaptive and responsive design;
* NPM та його модулі;
* Проект був зібраний за допомогою Gulp;

---

## Склад розробників проекту:
* ___Борисенко Микола___
* ___Поплавський Олександр___

---

## Список виконаних робіт:

* ___Поплавський Олександр___: 
    * Зверстав секцію `Revolutionary Editor`
   ![editor](/readme_assets/editor.png)
    * Зверстав секцію `Here is what you get`
   ![you-get](/readme_assets/you-get.png)
    * Зверстав секцію `Fork Subscription Pricing`
   ![pricing](/readme_assets/pricing.png)
* ___Борисенко Микола___:
    * Зверстав `nav-меню` сайту
    ![header](/readme_assets/header.png)
    ![header_mobile](/readme_assets/header_mobile.png)
    * Зверстав секцію `People Are Talking About Fork`
    ![about-fork](/readme_assets/about-fork.png)

---    

## Для запуску проекту використати команди:
    1. npm i
    2. gulp build
    3. gulp dev 

---

## GitLab pages

[step-project-forkio](https://poplavsky.ua.gitlab.io/step-project-forkio/)