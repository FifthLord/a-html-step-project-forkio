
export function addActiveHeader() {
   let btn = document.querySelector('.header__dropdown-burger');
   let menu = document.querySelector('.header__dropdown-menu');
   document.body.addEventListener('click', e => {
      if (e.target.closest('.header__dropdown-burger')) {
         btn.classList.toggle('active');
         menu.classList.toggle('active');
         Array.from(menu.children).forEach(i => i.classList.remove('active'));
         menu.addEventListener('click', e => {
            if (e.target.closest('.header__drop-menu-item')) {
               Array.from(menu.children).forEach(i => i.classList.remove('active'));
               e.target.classList.add('active');
            };
         });
      } else if (!e.target.closest('.header__nav')
         || e.target.closest('.header__nav-btn')
         || e.target.closest('.header__nav-logo')) {
         e.preventDefault();
         btn.classList.remove('active');
         menu.classList.remove('active');
      }
   });
};